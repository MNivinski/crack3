#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "bintree.h"

void insert(char* hash, node **leaf, char* word)
{
    if (*leaf == NULL)
    {
        *leaf = (node *) malloc(sizeof(node));
        (*leaf)->hash = (char*)malloc(sizeof(char) * (strlen(hash)+1));
        strcpy((*leaf)->hash, hash);
        (*leaf)->plaintext = (char*)malloc(sizeof(char) * (strlen(word)+1));
        strcpy((*leaf)->plaintext, word);
        /* initialize the children to null */ 
        (*leaf)->left = NULL;    
        (*leaf)->right = NULL;  
    }
    else if (strcmp(hash, (*leaf)->hash) < 0){//printf("insert left!\n");
        insert (hash, &((*leaf)->left), word);}
    else if (strcmp(hash, (*leaf)->hash) > 0){//printf("insert right!\n");
        insert (hash, &((*leaf)->right), word );} 
}

void print(node *leaf)
{
	if (leaf == NULL) return;
	if (leaf->left != NULL) print(leaf->left);
	printf("%s  %s\n", leaf->hash, leaf->plaintext);
	if (leaf->right != NULL) print(leaf->right);
}

// TODO: Modify so the key is the hash to search for
node *search(char* hash, node *leaf)
{
    if (leaf != NULL)
    {
        if (strcmp(hash, leaf->hash) == 0)
            return leaf;
        else if (strcmp(hash, leaf->hash) < 1)
            return search(hash, leaf->left);
        else
            return search(hash, leaf->right);
    }
    else return NULL;
}

/*
int main()
{
    node *tree = NULL;
    
    insert(5, &tree);
    insert(2, &tree);
    insert(8, &tree);
    insert(10, &tree);
    insert(1, &tree);
    insert(4, &tree);
    insert(8, &tree);
    insert(7, &tree);

    print(tree);
}
*/
