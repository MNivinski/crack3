#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "bintree.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    // Compare the two hashes
    if(strcmp(guess, hash) == 0) return 1;
    // Free any malloc'd memory
    return 0;
}

// TODO
// Read in the hash file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **read_hashes(char *filename)
{
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        printf("Can't open that file\n");
        exit(1);
    }
    int size = 50;
    
    char **strs = (char**)malloc(size * sizeof(char*));
    
    char str[100];
    int i = 0;
    while(fgets(str, 100, f) != NULL)
    {
        //printf("Partay %s\n", filename);
        str[strlen(str)-1] = '\0';
        if(i == size - 1)
        {
            size+=10;
            char **newarr = (char**)realloc(strs, size * sizeof(char*));
            if(newarr != NULL) strs = newarr;
            else exit(1);
            //printf("Partay2 %i\n", size);
        }
        char *newstr = (char*)malloc((strlen(str)+1)*sizeof(char));
        strcpy(newstr, str);
        strs[i] = newstr;
        i++;
    }
    strs[i] = NULL;
    return strs;
}


// TODO
// Read in the dictionary file and return the tree.
// Each node should contain both the hash and the
// plaintext word.
node *read_dict(char *filename)
{
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        printf("Can't open that file\n");
        exit(1);
    }
    node* head = NULL;
    char str[100];
    char hash[33];
    while(fscanf(f, "%s", str) != EOF)
    {
        strcpy(hash, md5(str, strlen(str)));
        insert(hash, &head, str);
    }
    //printf("here\n");
    return head;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);
    
    // TODO: Read the dictionary file into a binary tree
    node *dict = read_dict(argv[2]);

    // TODO
    // For each hash, search for it in the binary tree.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    int counter = 0;
    node* temp;
    while(hashes[counter] != NULL)
    {
        temp = search(hashes[counter], dict);
        printf("%s, %s\n", temp->hash, temp->plaintext);
        counter++;
    }
}
